<?php

namespace Potagercity\Swile;

use Omnipay\Common\AbstractGateway;
use Omnipay\Common\Http\ClientInterface;
use Omnipay\Common\Message\AbstractRequest;
use Omnipay\Common\Message\NotificationInterface;
use Omnipay\Common\Message\RequestInterface;
use Potagercity\Swile\Message\AuthorizeRequest;
use Potagercity\Swile\Message\CancelRequest;
use Potagercity\Swile\Message\CaptureRequest;
use Potagercity\Swile\Message\CheckMerchantsRequest;
use Potagercity\Swile\Message\EstimateChargesRequest;
use Potagercity\Swile\Message\MerchantsRequest;
use Potagercity\Swile\Message\PurchaseRequest;
use Potagercity\Swile\Message\RefundRequest;
use Symfony\Component\HttpFoundation\Request as HttpRequest;


/**
 * @method NotificationInterface acceptNotification(array $options = array())
 * @method RequestInterface completeAuthorize(array $options = array())
 * @method RequestInterface completePurchase(array $options = array())
 * @method RequestInterface fetchTransaction(array $options = [])
 * @method RequestInterface void(array $options = array())
 * @method RequestInterface createCard(array $options = array())
 * @method RequestInterface updateCard(array $options = array())
 * @method RequestInterface deleteCard(array $options = array())
 */
class Gateway extends AbstractGateway
{
    protected $serverUrl;


    public function __construct(ClientInterface $httpClient = null, HttpRequest $httpRequest = null)
    {
        parent::__construct($httpClient, $httpRequest);
    }

    public function setServerUrl($serverUrl)
    {
        $this->serverUrl = $serverUrl;
    }

    /**
     * @inheritdoc
     */
    public function getName(): string
    {
        return 'swile';
    }

    protected function getGlobalParameters(): array
    {
        return [
            'serverUrl' => $this->serverUrl,
        ];
    }

    public function authorize(array $options = array()): RequestInterface
    {
        $params = array_merge($options, $this->getGlobalParameters());

        return $this->createRequest(AuthorizeRequest::class, $params);
    }


    public function capture(array $options = array()): RequestInterface
    {
        $params = array_merge($options, $this->getGlobalParameters());

        return $this->createRequest(CaptureRequest::class, $params);
    }


    public function purchase(array $options = array()): RequestInterface
    {
        $params = array_merge($options, $this->getGlobalParameters());

        return $this->createRequest(PurchaseRequest::class, $params);
    }


    public function refund(array $options = array()): RequestInterface
    {
        $params = array_merge($options, $this->getGlobalParameters());

        return $this->createRequest(RefundRequest::class, $params);
    }

    public function cancel(array $parameters = array()): AbstractRequest
    {
        $params = array_merge($parameters, $this->getGlobalParameters());

        return $this->createRequest(CancelRequest::class, $params);
    }

    public function estimateCharges(array $parameters = array())
    {
        $params = array_merge($parameters, $this->getGlobalParameters());

        return $this->createRequest(EstimateChargesRequest::class, $params);
    }


    public function addMerchants(array $parameters = array())
    {
        $params = array_merge($this->getGlobalParameters(), $parameters);

        return $this->createRequest(MerchantsRequest::class, $params);
    }

    public function checkMerchantsRequest(array $parameters = array())
    {
        $params = array_merge($this->getGlobalParameters(), $parameters);

        return $this->createRequest(CheckMerchantsRequest::class, $params);
    }

    public function __call($name, $arguments)
    {
        // TODO: Implement @method \Omnipay\Common\Message\NotificationInterface acceptNotification(array $options = array())
        // TODO: Implement @method \Omnipay\Common\Message\RequestInterface completeAuthorize(array $options = array())
        // TODO: Implement @method \Omnipay\Common\Message\RequestInterface completePurchase(array $options = array())
        // TODO: Implement @method \Omnipay\Common\Message\RequestInterface fetchTransaction(array $options = [])
        // TODO: Implement @method \Omnipay\Common\Message\RequestInterface void(array $options = array())
        // TODO: Implement @method \Omnipay\Common\Message\RequestInterface createCard(array $options = array())
        // TODO: Implement @method \Omnipay\Common\Message\RequestInterface updateCard(array $options = array())
        // TODO: Implement @method \Omnipay\Common\Message\RequestInterface deleteCard(array $options = array())
    }
}

