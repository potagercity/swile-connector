<?php

/**
 * Stripe Capture Request.
 */

namespace Potagercity\Swile\Message;


class RefundRequest extends AbstractRequest
{
    public function getData(): array
    {
        $this->validate('refund');
        $this->validate('paymentUuid');

        $data = [];

        $capture = $this->getRefund();

        $this->setAmount($capture['amount']);

        $capture['amount'] = $this->getAmount();

        $data['refund'] = $capture;

        return $data;
    }

    public function getEndpoint(): string
    {
        return $this->endpoint . '/payments/' . $this->getPaymentUuid() . '/refund';
    }

    public function getHttpMethod(): string
    {
        return 'PUT';
    }

    /**
     * @param string $value
     *
     * @return AbstractRequest provides a fluent interface.
     */
    public function setOrderRef($value)
    {
        /**
         * SwileApi speak in cents
         */
        return $this->setParameter('orderRef', $value);
    }

    /**
     * @return mixed
     */
    public function getOrderRef()
    {
        return ($this->getParameter('orderRef'));
    }


    public function setaccountUuid($value)
    {
        return $this->setParameter('accountUuid', $value);
    }

    public function getaccountUuid()
    {
        return $this->getParameter('accountUuid');
    }

    public function setPaymentUuid($value)
    {
        return $this->setParameter('paymentUuid', $value);
    }

    public function getPaymentUuid()
    {
        return $this->getParameter('paymentUuid');
    }

    public function setRefund($value)
    {
        return $this->setParameter('refund', $value);
    }

    public function getRefund()
    {
        return $this->getParameter('refund');
    }
}

