<?php

/**
 * Stripe Capture Request.
 */

namespace Potagercity\Swile\Message;


class CheckMerchantsRequest extends AbstractRequest
{
    public function getData(): array
    {
        $this->validate('vendorId');

        return [];
    }

    public function getEndpoint(): string
    {
        return $this->endpoint . '/vendor/merchants/' . $this->getVendorId();
    }

    public function getHttpMethod(): string
    {
        return 'GET';
    }


    public function setVendorId($value)
    {
        return $this->setParameter('vendorId', $value);
    }

    public function getVendorId()
    {
        return $this->getParameter('vendorId');
    }
}

