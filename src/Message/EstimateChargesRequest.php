<?php

/**
 * Stripe Capture Request.
 */

namespace Potagercity\Swile\Message;


class EstimateChargesRequest extends AbstractRequest
{
    public function getData(): array
    {
        $this->validate('accountUuid');

        return [];
    }

    public function getEndpoint(): string
    {
        return $this->endpoint . '/accounts/' . $this->getaccountUuid() . '/wallet';
    }

    public function getHttpMethod(): string
    {
        return 'GET';
    }

    public function setaccountUuid($value)
    {
        return $this->setParameter('accountUuid', $value);
    }

    public function getaccountUuid()
    {
        return $this->getParameter('accountUuid');
    }
}

