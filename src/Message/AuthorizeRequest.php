<?php

/**
 * Stripe Capture Request.
 */

namespace Potagercity\Swile\Message;


class AuthorizeRequest extends AbstractRequest
{
    public function getData(): array
    {
        $this->validate('payment');
        $this->validate('accountUuid');

        $data = [];

        $payment = $this->getPayment();

        $this->setAmount($payment['amount']);

        $payment['amount'] = $this->getAmount();

        $data['payment'] = $payment;
        $data['only_authorize'] = $this->getOnlyAuthorize();

        return $data;
    }

    public function getEndpoint(): string
    {
        return $this->endpoint . '/accounts/' . $this->getaccountUuid() . '/payments';
    }

    public function getHttpMethod(): string
    {
        return 'POST';
    }

    public function setaccountUuid($value)
    {
        return $this->setParameter('accountUuid', $value);
    }

    public function getaccountUuid()
    {
        return $this->getParameter('accountUuid');
    }

    public function setPayment($value)
    {
        return $this->setParameter('payment', $value);
    }

    public function getPayment()
    {
        return $this->getParameter('payment');
    }

    public function setOnlyAuthorize($value)
    {
        return $this->setParameter('only_authorize', $value);
    }

    public function getOnlyAuthorize()
    {
        return $this->getParameter('only_authorize');
    }
}

