<?php

namespace Potagercity\Swile\Message;

use Omnipay\Common\Message\AbstractResponse;
use Omnipay\Common\Message\RedirectResponseInterface;
use Omnipay\Common\Message\RequestInterface;
use Throwable;

class Response extends AbstractResponse implements RedirectResponseInterface
{
    /**
     * Request id
     *
     * @var string URL
     */
    protected $requestId = null;

    /**
     * @var array
     */
    protected $headers = [];

    public function __construct(RequestInterface $request, $data, $headers = [])
    {
        parent::__construct($request, json_decode($data, true));
        $this->headers = $headers;
    }

    public function isSuccessful(): bool
    {
        try {
            $data = $this->getData();
            if (isset($data['payment']) || isset($data['wallet'])) {
                return true;
            }
        } catch (Throwable $t) {
            return false;
        }

        return false;
    }


}

