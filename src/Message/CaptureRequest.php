<?php

/**
 * Stripe Capture Request.
 */

namespace Potagercity\Swile\Message;


class CaptureRequest extends AbstractRequest
{
    public function getData(): array
    {
        $this->validate('capture');
        $this->validate('paymentUuid');

        $data = [];

        $capture = $this->getCapture();

        $this->setAmount($capture['amount']);

        $capture['amount'] = $this->getAmount();

        $data['capture'] = $capture;

        return $data;
    }

    public function getEndpoint(): string
    {
        return $this->endpoint . '/payments/' . $this->getPaymentUuid() . '/capture';
    }

    public function getHttpMethod(): string
    {
        return 'PUT';
    }

    /**
     * @param string $value
     *
     * @return AbstractRequest provides a fluent interface.
     */
    public function setOrderRef($value): AbstractRequest
    {
        /**
         * SwileApi speak in cents
         */
        return $this->setParameter('orderRef', $value);
    }

    /**
     * @return mixed
     */
    public function getOrderRef()
    {
        return ($this->getParameter('orderRef'));
    }


    public function setaccountUuid($value)
    {
        return $this->setParameter('accountUuid', $value);
    }

    public function getaccountUuid()
    {
        return $this->getParameter('accountUuid');
    }

    public function setPaymentUuid($value)
    {
        return $this->setParameter('paymentUuid', $value);
    }

    public function getPaymentUuid()
    {
        return $this->getParameter('paymentUuid');
    }

    public function setCapture($value)
    {
        return $this->setParameter('capture', $value);
    }

    public function getCapture()
    {
        return $this->getParameter('capture');
    }
}

