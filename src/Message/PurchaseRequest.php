<?php

/**
 * Stripe Capture Request.
 */

namespace Potagercity\Swile\Message;


class PurchaseRequest extends AuthorizeRequest
{

    public function getData(): array
    {
        $data = parent::getData();

        unset($data['only_authorize']);

        return $data;
    }


}
