<?php

/**
 * Stripe Capture Request.
 */

namespace Potagercity\Swile\Message;


class MerchantsRequest extends AbstractRequest
{
    public function getData(): array
    {
        $this->validate('merchant');

        $data = [];

        $merchant = $this->getMerchant();

        $data['merchant'] = $merchant;

        return $data;
    }

    public function getEndpoint(): string
    {
        return $this->endpoint . '/vendor/merchants';
    }

    public function getHttpMethod(): string
    {
        return 'POST';
    }


    public function setMerchant($value)
    {
        return $this->setParameter('merchant', $value);
    }

    public function getMerchant()
    {
        return $this->getParameter('merchant');
    }
}

