# Omnipay: Swile

**Swile driver for the Omnipay PHP payment processing library**

Composer update tag 1.1.0

## Installation

Omnipay is installed via [Composer](http://getcomposer.org/). To install, simply require `league/omnipay`
and `potagercity/swile` with Composer:

```
composer require league/omnipay potagercity/swile
```

List of available features :

- EstimateCharge
- Authorize
- Capture
- Refund / Cancel
- MerchantsRequest : list all merchants